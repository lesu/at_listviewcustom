package personal.listviewcustom;

/**
 * Created by Xu on 2016-01-19.
 */
 class RowData {
    private int image;
    private String mainText;
    private String secondaryText;

    RowData(int image, String mainText, String secondaryText) {
        this.image = image;
        this.mainText = mainText;
        this.secondaryText = secondaryText;
    }

    int getImage() {
        return image;
    }

     void setImage(int image) {
        this.image = image;
    }

     String getMainText() {
        return mainText;
    }

     void setMainText(String mainText) {
        this.mainText = mainText;
    }

     String getSecondaryText() {
        return secondaryText;
    }

     void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }
}
