package personal.listviewcustom;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Random;

/**
 * <a href="http://androidexample.com/How_To_Create_A_Custom_Listview_-_Android_Example/index.php?view=article_discription&aid=67&aaid=92">androidexample.com | How To Create A Custom Listview - Android Example</a>
 */
public class MainActivity extends AppCompatActivity {
    private Button buttonAdd;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }


    private void initComponents() {
        this.listView = (ListView) this.findViewById(R.id.list_view_items);
        this.buttonAdd = (Button) this.findViewById(R.id.button_add);
        this.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RowAdapter rowAdapter = new RowAdapter(MainActivity.this.getApplicationContext(),MainActivity.this.getRowDataList());
                MainActivity.this.listView.setAdapter(rowAdapter);
            }
        });
    }



    private ArrayList<RowData> getRowDataList() {
        ArrayList<RowData> result = new ArrayList<RowData>();

        final String even = "Even";
        final String odd = "Odd";

        for (int i=0 ; i < 20 ; i++) {
            int num = new Random().nextInt(Short.MAX_VALUE);
            if(num%2==0) {
                result.add(new RowData(R.drawable.ii,even,String.valueOf(num)));
            } else {
                result.add(new RowData(R.drawable.i,odd,String.valueOf(num)));
            }
        }

        return result;
    }
}
